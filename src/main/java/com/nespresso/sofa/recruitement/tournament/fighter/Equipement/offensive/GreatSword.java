package com.nespresso.sofa.recruitement.tournament.fighter.Equipement.offensive;

public class GreatSword implements Weapon {
    private static final int MAKED_DAMAGES = 12;
    private int attackTimePerThree = 1;

    public int damage() {
        if (attackTimePerThree < 3) {
            attackTimePerThree++;
            return MAKED_DAMAGES;
        } else {
            attackTimePerThree = 1;
        }
        return 0;
    }

}
