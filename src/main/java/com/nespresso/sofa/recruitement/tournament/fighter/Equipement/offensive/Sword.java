package com.nespresso.sofa.recruitement.tournament.fighter.Equipement.offensive;

public class Sword implements Weapon {
    private static final int MAKING_DAMAGES = 5;

    public int damage() {
        return MAKING_DAMAGES;
    }
}
