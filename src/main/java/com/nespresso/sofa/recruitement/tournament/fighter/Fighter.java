package com.nespresso.sofa.recruitement.tournament.fighter;

import com.nespresso.sofa.recruitement.tournament.fighter.Equipement.EquipementFactory;
import com.nespresso.sofa.recruitement.tournament.fighter.Equipement.defensive.Defensive;
import com.nespresso.sofa.recruitement.tournament.fighter.Equipement.offensive.Weapon;

public abstract class Fighter {
    protected int hitPoints;
    protected Defensive defensive;
    protected Weapon weapon;

    public void engage(Fighter fighter) {
        if (isAlive()) {
            fighter.hitPoints = Math.max(0, fighter.hitPoints - fighter.defence(offense(), this));
            fighter.engage(this);
        }
    }

    private int defence(int i, Fighter fighter) {
        if (defensive != null) {
            return defensive.defenseFromAttack(i, fighter.weapon);
        }
        return i;
    }

    private boolean isAlive() {
        return (hitPoints > 0);
    }

    public int hitPoints() {
        return this.hitPoints;
    }

    public int offense() {
        if (defensive != null) {
            return Math.max(0, weapon.damage() - defensive.penaltiesOfUse());
        }
        return weapon.damage();
    }

    public Fighter equip(String equipements) {
        EquipementFactory.createEquipementTo(equipements, this);
        return this;
    }

    public void addDefensive(Defensive defensive) {
        defensive.setPreviousDefensive(this.defensive);
        this.defensive = defensive;
    }

    public void addOffensive(Weapon weapon) {
        this.weapon = weapon;
    }
}
