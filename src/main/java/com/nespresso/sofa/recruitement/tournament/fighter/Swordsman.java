package com.nespresso.sofa.recruitement.tournament.fighter;

import com.nespresso.sofa.recruitement.tournament.fighter.Equipement.offensive.Poison;
import com.nespresso.sofa.recruitement.tournament.fighter.Equipement.offensive.Sword;
import com.nespresso.sofa.recruitement.tournament.fighter.Equipement.offensive.Weapon;

public class Swordsman extends Fighter {
    private static final int INITIAL_HIT_POINTS = 100;
    private boolean isVicious;

    public Swordsman() {
        this("notVicious");
    }

    public Swordsman(String state) {
        if (state.equals("Vicious")) isVicious = true;
        this.hitPoints = INITIAL_HIT_POINTS;
        this.weapon = new Sword();
    }

    @Override
    public Swordsman equip(String equipements) {
        return (Swordsman) super.equip(equipements);
    }

    @Override
    public void addOffensive(Weapon weapon) {
        super.addOffensive(weapon);
        if (isVicious) {
            this.weapon = new Poison(this.weapon);
        }
    }
}
