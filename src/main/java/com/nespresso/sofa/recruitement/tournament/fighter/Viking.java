package com.nespresso.sofa.recruitement.tournament.fighter;

import com.nespresso.sofa.recruitement.tournament.fighter.Equipement.offensive.Axe;

public class Viking extends Fighter {
    private static final int INITIAL_HIT_POINTS = 120;

    public Viking() {
        this.hitPoints = INITIAL_HIT_POINTS;
        this.weapon = new Axe();
    }

    @Override
    public Viking equip(String equipements) {
        return (Viking) super.equip(equipements);
    }
}
